package sk.ardevop.order;

import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import sk.ardevop.productcatalog.model.ProductRto;

@Component
@Slf4j
public class ProductsApiFallback implements ProductsApiClient {

  @Override
  public ResponseEntity<List<ProductRto>> findProducts() {
    log.info("Returning fake implementation");
    return ResponseEntity.ok(Collections.singletonList(ProductRto.builder()
        .id("0")
        .category("fake category")
        .name("fake name")
        .price(1f).build()
    ));
  }
}
