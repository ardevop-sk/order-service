package sk.ardevop.order;

import org.springframework.cloud.openfeign.FeignClient;
import sk.ardevop.productcatalog.api.DefaultApi;

@FeignClient(name="product-catalog", fallback = ProductsApiFallback.class)
public interface ProductsApiClient extends DefaultApi {

}
